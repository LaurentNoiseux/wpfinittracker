﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InitTrackerUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        public List<TextBlock> positionList = new List<TextBlock>();
        public List<Creature> creatureList = new List<Creature>();

        private bool isCreatureTabOpen = false;
        private int lowestInit;
        private int highestInit;




        public MainWindow()
        {
            InitializeComponent();
            PopulatePositionList();
        }
        private void PopulatePositionList()
        {
            positionList.Add(Position1);
            positionList.Add(Position2);
            positionList.Add(Position3);
            positionList.Add(Position4);
            positionList.Add(Position5);
            positionList.Add(Position6);
            positionList.Add(Position7);
            positionList.Add(Position8);
            positionList.Add(Position9);
            positionList.Add(Position10);
            positionList.Add(Position11);
            positionList.Add(Position12);
            positionList.Add(Position13);
            positionList.Add(Position14);
            positionList.Add(Position15);
            positionList.Add(Position16);
            positionList.Add(Position17);
            positionList.Add(Position18);
            positionList.Add(Position19);
            positionList.Add(Position20);
        }

        private void OpenPanelCreatureButton_Click(object sender, RoutedEventArgs e)
        {
            
            if(!isCreatureTabOpen)
            {
                isCreatureTabOpen = true;
                Storyboard sb = Resources["OpenAddCreaturePanel"] as Storyboard;
                sb.Begin();

            }
            else
            {
                isCreatureTabOpen = false;
                Storyboard sb = Resources["CloseAddCreaturePanel"] as Storyboard;
                sb.Begin();
            }

        }
        private void AddCreatureButton_Click(object sender, RoutedEventArgs e)
        {
            
            try
            {   Creature newCreature;
                newCreature = new Creature(NameInput.Text, int.Parse(InitInput.Text), int.Parse(HpInput.Text));
                SortCreature(newCreature);
            }
            catch (Exception except)
            {
                MessageBox.Show("Invalid Input");
            }
        }

        private void ActionButton_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                // Case where action is damage
                if (DamageRadioButton.IsChecked == true)
                {
                    int damageResult = creatureList[TargetComboBox.SelectedIndex].ReceiveDamage(int.Parse(ValueInput.Text));

                    // Case where target dies
                    if (damageResult == 1)
                    {
                        creatureList.RemoveAt(TargetComboBox.SelectedIndex);
                    }
                }

                // Case where action is healing
                else
                {
                    creatureList[TargetComboBox.SelectedIndex].ReceiveHealing(int.Parse(ValueInput.Text));
                }
            }catch(Exception excep)
            {
                MessageBox.Show("Invalid damage/healing value");
            }

            PrintCreature();
        }

        private void SortCreature(Creature newCreature)
        {
            // Creature list is empty. Add the creature immediatly
            if(creatureList.Count == 0)
            {
                creatureList.Add(newCreature);
                highestInit = newCreature.GetInit();
                lowestInit = newCreature.GetInit();
            }

            // newCreature has the highest init
            else if (newCreature.GetInit() >= highestInit)
            {
                creatureList.Insert(0, newCreature);
                highestInit = newCreature.GetInit();
            }

            // newCreature has the lowest init
            else if (newCreature.GetInit() < lowestInit)
            {
                creatureList.Add(newCreature);
                lowestInit = newCreature.GetInit();
            }

            // newCreature has to be added to the middle of the list
            else
            {
                AddMiddle(newCreature);
            }

            PrintCreature();
        }
        private void AddMiddle(Creature newCreature)
        {
            bool positionFound = false;
            int aimedPosition = 0;
            int positionSearch = 1;
            while (!positionFound)
            {
                if (newCreature.GetInit() >= creatureList[positionSearch].GetInit())
                {
                    positionFound = true;
                    aimedPosition = positionSearch;
                }
                else
                {
                    positionSearch++;
                }
            }
            creatureList.Insert(aimedPosition, newCreature);
        }


        private void PrintCreature()
        {
            ClearText();
            for(int i =0; i<creatureList.Count; i++){
                positionList[i].Text = creatureList[i].GetPrintText();
            }
            RefeshComboBox();
        }
        private void RefeshComboBox()
        {
            
            TargetComboBox.ItemsSource = creatureList;
            if (creatureList.Count == 0)
            {
                TargetComboBox.SelectedItem = null;
            }
            else
            {
                TargetComboBox.SelectedIndex = 0;
            }


        }
        private void ClearText()
        {
            foreach(TextBlock tb in positionList)
            {
                tb.Text = " ";
            }
        }


    }

}
