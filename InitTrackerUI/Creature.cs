﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InitTrackerUI
{
    public class Creature
    {
        private string name;
        private string printText;
        private int init;
        private int maxHp;
        private int currentHp;


        //Empty constructor
        public Creature(string newName, int newInit, int newMaxHp)
        {
            name = newName;
            init = newInit;
            maxHp = newMaxHp;
            currentHp = newMaxHp;

            printText = name + " " + currentHp;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="damageValue"></param>
        /// <returns> 1 if creature is dead. 0 otherwise</returns>
        public int ReceiveDamage(int damageValue)
        {
            currentHp = currentHp - damageValue;
            
            if (currentHp <= 0)
            {
                return 1;
            }
            else
            {
                UpdatePrintText();
                return 0;
            }
            
        }

        public void ReceiveHealing(int healValue)
        {
            currentHp = currentHp + healValue;
            if (currentHp > maxHp)
            {
                currentHp = maxHp;
            }
            UpdatePrintText();
        }

        public void UpdatePrintText()
        {
            printText = name + " " + currentHp;
        }

        //Getters
        public string GetName()
        {
            return name;
        }
        public string GetPrintText()
        {
            return printText;
        }
        public int GetInit()
        {
            return init;
        }
        public int GetHp()
        {
            return maxHp;
        }
        
        // Getter for the UI
        public string Name
        {
            get
            {
                return name;
            }
        }
       
        //Setters
        public void SetName(string newName)
        {
            name = newName;
        }
        public void SetInit(int newInit)
        {
            init = newInit;
        }
        public void SetMaxHp(int newMaxHp)
        {
            maxHp = newMaxHp;
        }

    }
}
